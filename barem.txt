   ##########

      # Menus: #

      ##########

		------------------------------------------------------------------------------------------------------------------------------------------------

		Nouveau

		Nouvelle fenêtre

		Ouvrir...

		Enregistrer

		Enregistrer sous...

		Quitter






		------------------------------------------------------------------------------------------------------------------------------------------------

		Couper

1    pts		Combinaison de touches (raccourci)

			Actions:

1    pts			S'active seulement si du texte est sélectionné et se désactive s'il n'y a plus rien de sélectionné

1    pts			Action du "Couper" sur le JTextArea (donc coupe seulement la partie sélectionnée du JTextArea)

		------------------------------------------------------------------------------------------------------------------------------------------------







		------------------------------------------------------------------------------------------------------------------------------------------------

		Copier

1    pts		Combinaison de touches (raccourci)

			Actions:

1    pts			S'active seulement si du texte est sélectionné et se désactive s'il n'y a plus rien de sélectionné

1    pts			Action du "Copier" sur le JTextArea (donc copie seulement la partie sélectionnée du JTextArea)

		------------------------------------------------------------------------------------------------------------------------------------------------







		------------------------------------------------------------------------------------------------------------------------------------------------

		Coller

1    pts		Combinaison de touches (raccourci)

1    pts		Action du "Coller" sur le JTextArea (donc insère le texte là où se trouve le caret du JTextArea)



		(Caret: c'est le curseur clignotant qui pointe la position du texte que vous écrivez)

		------------------------------------------------------------------------------------------------------------------------------------------------


		Supprimer

		Sélectionner tout

		Heure/Date

		Retour à la ligne automatique

		Barre d'état

